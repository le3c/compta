class CreateTickets < ActiveRecord::Migration
  def change
    create_table :tickets do |t|
      t.integer :numéro
      t.datetime :date
      t.decimal :ttc20
      t.decimal :ttc5
      t.decimal :caisseréelle

      t.timestamps null: false
    end
    add_index :tickets, :numéro, unique: true
  end
end
