class Ticket < ActiveRecord::Base

  attr_accessor :date_par_défaut

#  def initialize(attributes = { date: Date.today })
#    @date = attributes[:date]
#  end

  def ht20
    @ht20 = (ttc20 / 1.2).round(2)
  end

  def ht5
    @ht5 = (ttc5 / 1.055).round(2)
  end

  def tva20
    @tva20 = ttc20 - ht20
  end

  def tva5
    @tva5 = ttc5 - ht5
  end


  def totalTTC
    @totalTTC = ttc20 + ttc5
  end

  def trou_de_caisse
    @trou_de_caisse = caisseréelle - totalTTC
  end

  def self.to_csv
    CSV.generate(:col_sep => ";") do |csv|
      all.each do |t|
        attributes = [:date, :numéro, :totalTTC, :ht5, :ht20, :tva5, :tva20, :trou_de_caisse] 
        valeurs = attributes.inject({}) { |hash, item| hash[item] = t.send(item); hash }
        valeurs[:date] = valeurs[:date].strftime("%d/%m/%y")
        # les colonnes sont: journal, date, compte, pièce, crédit, débit
 	      ligne = []
        ligne[0] = [ 'AC', valeurs[:date], 531, valeurs[:numéro], valeurs[:totalTTC], 0 ]
        ligne[1] = [ 'AC', valeurs[:date], 707, valeurs[:numéro], 0, valeurs[:ht20] ]
        ligne[2] = [ 'AC', valeurs[:date], 44571101, valeurs[:numéro], 0, valeurs[:tva20] ]
        ligne[3] = [ 'AC', valeurs[:date], 707, valeurs[:numéro], 0, valeurs[:ht5] ]
        ligne[4] = [ 'AC', valeurs[:date], 4457111, valeurs[:numéro], 0, valeurs[:tva5] ]

        # s'il n'y a pas de trou de caisse, on n'écrit aucune ligne
        # parce qu'EBP n'accepte pas d'importer des lignes avec un montant à 0
        if valeurs[:trou_de_caisse] != 0
          # si le trou de caisse est négatif, la valeur doit aller au crédit en ligne 5 et débit ligne 6, sinon l'inverse
          tab = [valeurs[:trou_de_caisse].abs, 0]
          # si le trou de caisse est négatif le compte doit être 658, et s'il est positif 758
          compte_trou_de_caisse = 658
          if valeurs[:trou_de_caisse] > 0 
            # inversion des valeurs du tableau. y'a sûrement plus joli…
           tab.push(tab[0])
           tab.delete_at(0) 
           compte_trou_de_caisse = 758
          end
  
          ligne[5] = [ 'OD', valeurs[:date], compte_trou_de_caisse, valeurs[:numéro], tab[0], tab[1] ]
          ligne[6] = [ 'OD', valeurs[:date], 531, valeurs[:numéro], tab[1], tab[0] ]

        end

        ligne.each do |l|
          csv << l
        end

      end



    end
  end


end
