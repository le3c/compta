class TicketsController < ApplicationController

  def index
    @all_tickets = Ticket.all
    respond_to do |format|
      format.csv { send_data @all_tickets.to_csv, filename: "tickets_mois.csv" }
    end
  end

  def show
    @ticket = Ticket.find(params[:id])
  end
  
  def new
    if Ticket.last
      @lastticket = Ticket.last 
      @date_dernier_ticket = @lastticket.date
    else
      @date_dernier_ticket = Date.today - 1.day
    end
    @newticket = Ticket.new(:date_par_défaut => prochain_jour_ouvert(@date_dernier_ticket))
  end

  def create
    @ticket = Ticket.new(ticket_params)
    @ticket.date = Date.civil(params[:date][:year].to_i, params[:date][:month].to_i, params[:date][:day].to_i)
    if @ticket.save
      # redirect_to ticket_url(@ticket)    anciennement la page du ticket créé, changé pour rediriger vers la page new
      redirect_to new_ticket_url
    end
  end

  def ticket_params
    params.require(:ticket).permit(:numéro, :date, :ttc20, :ttc5, :caisseréelle)
  end

  
  # le prochain jour où le café est ouvert
  def prochain_jour_ouvert(datein)
    if datein.saturday?
      datein + 3.day
    elsif datein.sunday?
      datein + 2.day
    else
      datein + 1.day
    end
  end


end
